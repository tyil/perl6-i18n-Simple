#! /usr/bin/env false

use v6.c;

unit class I18n::Simple::Template;

multi method parse (
	Str:D $input,
	%context,
	--> Str
) {
	$input.subst(
		/ '$(' ( <[\w -]>+ ) ')' /,
		{ %context{$0} // die "Unknown variable used: $0" },
		:g
	);
}

=begin pod

=NAME    I18n::Simple::Template
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
