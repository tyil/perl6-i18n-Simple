#! /usr/bin/env false

use v6.c;

use Hash::Merge;
use I18n::Simple::Template;
use YAMLish;

unit module I18n::Simple;

my %templates;

#| Load in the translation templates defined in a given file.
multi sub i18n-init (
	IO::Path:D $path,
	Bool:D :$validate = True,
) is export {
	my %yaml = $path.slurp.&load-yaml;

	%yaml.keys.map({ die "{$_}'s value is not a string" unless %yaml{$_} ~~ Str }) if $validate;

	%templates = merge-hashes(%templates, %yaml);
}

multi sub i18n-init (
	Distribution::Resource:D $resource,
	Bool:D :$validate = True,
) is export {
	samewith($resource.IO, :$validate);
}

multi sub i18n-init (
	Str:D $path,
	Bool:D :$validate = True,
) is export {
	samewith($path.IO, :$validate);
}

#| Load in a template, and apply the given context to it, then return the end
#| result.
sub i18n (
	Str:D $template,
	*%context,
	--> Str
) is export {
	return %templates{$template}.trim unless %context;

	I18n::Simple::Template.parse(%templates{$template}, %context).trim;
}

=begin pod

=NAME    I18n::Simple
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.2

=head1 Description

Employ easy-to-use templating for internationalisation in your code.

=head1 Examples

Take the following YAML file, and save it in your module's C<resources>
directory. These examples assume the file being saved at
C<resources/i18n/en.yml>.

=begin code
---
foo: This is a template!
bar: This template is for you, $(user).
=end code

Then use the module as follows:

=begin code
use I18n::Simple;

# Load the templates. You can make the `en` part a variable to easily switch
# between different languages in your program.
i18n-init(%?RESOURCES<i18n/en.yml>);

# Call a template without placeholders
dd i18n("foo"); # "This is a template!"

# Call a template with placeholders
dd i18n("bar", user => "Bob"); # "This template is for you, Bob."

=end code

=end pod

# vim: ft=perl6 noet
