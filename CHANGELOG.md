# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2018-09-04
### Changed
- Updated a test to not rely on the `$USER` environment variable, as this fails
  in the default `rakudo-star` image which does not have it set.

## [0.1.0] - 2018-08-28
- Initial release
