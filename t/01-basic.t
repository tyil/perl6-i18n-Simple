#! /usr/bin/env perl6

use v6.c;

use Test;
use I18n::Simple;

plan 3;

ok i18n-init("t/en.yml"), "Initializes ok";

is i18n("test"), "This is some testing text.", "Template without placeholders is correct";

subtest "Passing context as named arguments", {
	plan 1;

	is i18n("other.test", placeholder => "Placeholder", name => "Larry"),
		"Now some test with a Placeholder. Hope this shows your name, Larry.",
		"Template with placeholders is correct";
}

# vim: ft=perl6 noet
